public class Main {
    public static void main(String[] args) {
        // Question 1: Creating a student with valid values
        Student alice = new Student("Alice", 20, "Computer Science", 3.5, 10000);
        System.out.println(alice.getName() + " is a " + alice.getMajor() + " major with a GPA of " +
                alice.getGpa() + " and a tuition of $" + alice.getTuition());

        // Question 1: Creating a student with an invalid age
        try {
            Student bob = new Student("Bob", -10, "Mathematics", 2.7, 12000);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        // Question 2: Using the FixedAmountScholarship strategy
        ScholarshipDecorator scholarship = new FixedAmountScholarship(5000, alice);
        System.out.println(alice.getName() + "'s tuition after the scholarship is $" +
                scholarship.getTuition());

        // Question 3: Using the printDetails method with a Person object
        Person charlie = new Person("Charlie", 30);
        charlie.printDetails();

        // Question 3: Using the printDetails method with a Student object
        alice.printDetails();

        // Question 3: Using the printDetails method with a decorated Student object
        scholarship.printDetails();

        // Question 4: Using the GPAStatusTracker observer
        GPAStatusTracker statusTracker = new GPAStatusTracker();
        Student david = new Student("David", 22, "English", 1.9, 8000);
        david.addObserver(statusTracker);
        david.setGpa(2.5);
        david.setGpa(1.8);
        david.setGpa(3.0);
    }

}